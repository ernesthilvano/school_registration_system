# School Registration System

Design and implement simple school registration system
- Assuming you already have a list of students
- Assuming you already have a list of courses
- A student can register to multiple courses
- A course can have multiple students enrolled in it.
- A course has 50 students maximum
- A student can register to 5 course maximum

Provide the following REST API:
- Create students CRUD
- Create courses CRUD
- Create API for students to register to courses
- Create abilities for user to view all relationships between students and courses
+ Filter all students with a specific course
+ Filter all courses for a specific student
+ Filter all courses without any students
+ Filter all students without any courses

## Assumptions

1. Maximum of 50 students per course
2. Student cannot have duplicate email
3. Course cannot have duplicate name
4. Student can have maximum of 5 course

## Solution formulation

1. course student count is being evaluated first before enrolling new student
2. enrolling student requires unique email which can be evaluated during creation of new student
3. creating new course requires unique name which can also be evaluated during creation of new course
4. student course size is being evaluated first before allowing student to enroll for the course

## Validations
1. Student email should be unique and is valid format
2. Course name should be unique
3. Student max course allowed
4. Course max student allowed
5. Student cannot subscribe twice to same course
6. Using Invalid student id or course id throws EntityNotFoundException

## Libraries/Tools used

* Spring boot
* Maven
* Swagger UI
* Docker (docker-compose)
* JUnit / Mockito
* MySQL / Hibernate

## Entity Setup
1. Student Request used for creating or updating new student

```
    StudentRequest  {
        email	string
        firstName	    string
        lastName	    string
    }

    Sample:
       {
         "email": "test@gmail.com",
         "firstName": "testFirstName",
         "lastName": "testLastName"
       }

   Sample Response
       {
            "id":1,
            "firstName":"testFirstName",
            "lastName":"testLastName",
            "email":"test@gmail.com"
       }
```

2. Course Request used for creating or updating new course
```
    CourseRequest   {
        description	    string
        name	        string
    }

    Sample:
       {
         "name": "ARTS101",
         "description": "Arts and Literature"
       }

    Sample Response:
       {
         "id":1,
         "name": "ARTS101",
         "description": "Arts and Literature"
       }

```


## How to setup

Pre Requisite
1. Clone the project using `git clone git@gitlab.com:ernesthilvano/school_registration_system.git`
2. Ensure you are on main branch `git checkout main`

Currently there are two ways to run the project, Assuming you have `mvn` and/or `docker` installed First option is to try Running as stand alone using in memory db
1.    run `mvn spring-boot:run`

Second option: Running via docker-compose that has docker container for app and mysql along with initial database configuration.
1. run `docker-compose build`
2. run `docker-compose up` 

To verify you may want to try the ff options:
1. Access swagger UI via: http://localhost:8080/swagger-ui.html
2. access api via `curl localhost:8080/students`
    - Retrieving students should return students added from database creation script initial data (init.sql)
3. access api via postman

## Docker compose
Docker compose contains the initial script from init.sql where database is being created, new user is being added, new table setup and adding initial data. only once.

## Running tests

* To run the tests run `mvn test` on the project root.
* Unit tests focuses on CourseService and StudentService where we have the business logic.
* Minimal unit test for controller is also being implemented as part of having unit test per layer/component.
* Unit test coverage is 42% for service package.
![Code Coverage](https://gitlab.com/ernesthilvano/school_registration_system/-/blob/main/image.png)

## Decisions and tradeoffs

1. Filtering students with no courses API Path is currently students/no_courses same for retrieving courses with no students, this is to simplify the implementation given that we only have that specific filtering requirements.
this design limits possibility of implementing other filtering requirements due to having specific endpoint. better approach is to have a search api for students e.g /students/search
where we can have more fine grained search functionality.

## If it was a bigger project

This is a coding challenge and scope is quite small. If it was a bigger real project, doing the following would be better:

1. More focus on architecture and software design would be necesssary specially identifying the scope for additional filtering functionalities.
2. OAUTH2 can be used for securing the API
3. Add more audit fields for student and course entity e.g date_created and date_updated and more related fields needed for audits.
4. make unit test coverage 100%


## Acceptance criteria
### Student CRUD
```
* Retrieve All Student                    GET               /students
* Create Student -                        POST              /students
    Request: 
                                            {
                                                "email": "test@gmail.com",
                                                "firstName": "testFirstName",
                                                "lastName": "testLastName"
                                            }

* Update Student -                        PUT               /students/{student_id}
    Request: 
                                            {
                                                "email": "newEmail@gmail.com",
                                                "firstName": "newFirstName",
                                                "lastName": "newLastName"
                                            }     

* Delete Student using ID                 DELETE            /students/{student_id}
* Retrieve Student using ID               GET               /students/{student_id}
* Retrieve courses by student             GET               /students/{student_id}/courses
* Retrieve students subscribe to course   GET               /students/no_courses 
``` 

### Course CRUD
```
* Retrieve All Course                    GET                /courses
* Create Course -                       POST                /courses
    Request: 
                                            {
                                                "name": "ARTS101",
                                                "description": "Arts and Literature",
                                            }  

* Update Course -                        PUT                /courses/{course_id}
    Request: 
                                            {
                                                "name": "ARTS101",
                                                "description": "Arts and Literature new Description",
                                            }       

* Delete Course using ID                 DELETE             /courses/{course_id}
* Retrieve Course using ID               GET                /courses/{course_id}
* Subscribe student to course            POST               /courses/{course_id}/students/{student_id}
* Delete student from course             DELETE             /courses/{course_id}/students/{student_id}
* Retrieve students by course            GET                /courses/students/{student_id}
* Retrieve courses without students      GET                /courses/no_students
```

   
