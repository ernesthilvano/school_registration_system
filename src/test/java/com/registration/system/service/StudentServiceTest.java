package com.registration.system.service;

import com.registration.system.exception.ApiException;
import com.registration.system.model.Course;
import com.registration.system.model.Student;
import com.registration.system.repository.CourseRepository;
import com.registration.system.repository.StudentRepository;
import com.registration.system.request.StudentRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentServiceTest {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    StudentService studentService;

    @Test
    public void testGetAllStudents() {
        Assert.assertTrue( studentService.getAllStudents().size() > 1);
    }

    @Test
    public void testSaveNewStudentOnce_should_success() {
        StudentRequest studentRequest = new StudentRequest();
        studentRequest.setEmail("studentRequest5@gmail.com");
        studentRequest.setFirstName("studentFirstName1");
        studentRequest.setLastName("studentLastName1");

        studentService.saveStudent(studentRequest);
        List<Student> students = studentService.getAllStudents();
        Student student = students.get(students.size() - 1); //get last added
        Assert.assertEquals(studentRequest.getEmail(), student.getEmail());
        Assert.assertEquals(studentRequest.getFirstName(), student.getFirstName());
        Assert.assertEquals(studentRequest.getLastName(), student.getLastName());
    }

    @Test(expected = EntityExistsException.class)
    public void testSaveNewStudentDuplicateEmail_should_return_error() {
        StudentRequest studentRequest = new StudentRequest();
        studentRequest.setEmail("studentRequest5@gmail.com");
        studentRequest.setFirstName("studentFirstName1");
        studentRequest.setLastName("studentLastName1");

        studentService.saveStudent(studentRequest);
        studentService.saveStudent(studentRequest);
    }

    @Test
    public void testUpdateExistingStudent_should_success() {
        StudentRequest studentRequest = new StudentRequest();
        studentRequest.setEmail("studentRequest1@gmail.com");
        studentRequest.setFirstName("newFirstName");
        studentRequest.setLastName("newLastName");
        studentService.updateStudent(1, studentRequest);

        Student expected = studentService.getStudentById(1);
        Assert.assertEquals(studentRequest.getFirstName(), expected.getFirstName());
        Assert.assertEquals(studentRequest.getLastName(), expected.getLastName());
        Assert.assertEquals(studentRequest.getEmail(), expected.getEmail());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateNonExistingStudent_should_return_error() {
        StudentRequest studentRequest = new StudentRequest();
        studentRequest.setEmail("studentRequest1@gmail.com");
        studentRequest.setFirstName("newFirstName");
        studentRequest.setLastName("newLastName");
        studentService.updateStudent(99, studentRequest);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNonExistingStudent_should_return_error() throws ApiException {
        studentService.deleteStudent(99);
    }

    @Test(expected = ApiException.class)
    public void testDeleteExistingStudentWithCOurse_should_return_error() throws ApiException {
        studentService.deleteStudent(1);
    }

    @Test
    public void testDeleteExistingStudent_should_success() throws ApiException {
        List<Student> students = studentService.getAllStudents();
        int currentSize = studentService.getAllStudents().size();
        studentService.deleteStudent(students.get(currentSize - 1).getId()); //remove latest added
        int newSize = studentService.getAllStudents().size();
        Assert.assertEquals(currentSize - 1, newSize);
    }

    @Test
    public void testGetCourseByStudentId_should_success() {
        Set<Course> courses = studentService.getCourseByStudentId(1);
        Assert.assertTrue(courses.size() >= 1);

    }

    @Test
    public void testGetStudentsSubscribedCourses_should_success() {
        Set<Student> students = studentService.getStudentsSubscribed(1);
        Assert.assertTrue(students.size() >= 1);
    }

    @Test
    public void testGetStudentsNoSubscribedCourses_should_success() {
        List<Student> courses = studentService.getStudentsNotSubscribed();
        Assert.assertEquals(2, courses.size());
    }

}
