package com.registration.system.service;

import com.registration.system.exception.ApiException;
import com.registration.system.model.Course;
import com.registration.system.model.Student;
import com.registration.system.repository.CourseRepository;
import com.registration.system.repository.StudentRepository;
import com.registration.system.request.CourseRequest;
import com.registration.system.request.StudentRequest;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseServiceTest {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    CourseService courseService;

    @Test
    public void testGetAllCourses() {
        Assert.assertTrue( courseService.getAllCourses().size() > 1);
    }

    @Test
    public void testSaveNewCourseOnce_should_return_error() {
        int currentCourses = courseService.getAllCourses().size();
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setName("ALGORITHMS 101");
        courseRequest.setDescription("Algorithms for beginners");

        courseService.saveCourse(courseRequest);
        Assert.assertEquals(currentCourses + 1, courseService.getAllCourses().size());
    }

    @Test
    public void testGetCourse_should_success() {
        Assert.assertTrue(courseService.getCourseById(1) != null);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCourseInvalidId_should_return_error() {
        courseService.getCourseById(999);
    }

    @Test
    public void testUpdateExistingCourse_should_success() {
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setName("MATH001");
        courseRequest.setDescription("Mathematics basic algebra");
        courseService.updateCourse(1, courseRequest);

        Course expected = courseService.getCourseById(1);
        Assert.assertEquals(courseRequest.getName(), expected.getCourseName());
        Assert.assertEquals(courseRequest.getDescription(), expected.getDescription());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateNonExistingCourse_should_return_error() {
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setName("MATH001");
        courseRequest.setDescription("Mathematics basic algebra");
        courseService.updateCourse(1000, courseRequest);
    }

    @Test(expected = ApiException.class)
    public void testDeleteCourseWithStudents_should_return_error() throws ApiException {
        courseService.deleteCourse(2);
    }


    @Test(expected = EntityExistsException.class)
    public void testSaveNewCourseTwice_should_fail() {
        CourseRequest courseRequest = new CourseRequest();
        courseRequest.setName("PEFootBall");
        courseRequest.setDescription("Sports Activity for students");

        courseService.saveCourse(courseRequest);
        courseService.saveCourse(courseRequest);
    }

    @Test(expected = ApiException.class)
    public void testCourseOverSubscribe_should_return_error() throws ApiException {
        Student studentRecord1 = new Student("studentFirstName1", "studentLastName1", "studentEmail1@gmail.com");
        Student studentRecord2 = new Student("studentFirstName2", "studentLastName2", "studentEmail2@gmail.com");
        Student studentRecord3 = new Student("studentFirstName3", "studentLastName3", "studentEmail3@gmail.com");
        Student studentRecord4 = new Student("studentFirstName4", "studentLastName4", "studentEmail4@gmail.com");

        studentRepository.save(studentRecord1);
        studentRepository.save(studentRecord2);
        studentRepository.save(studentRecord3);
        studentRepository.save(studentRecord4);

        courseService.subscribeStudentToCourse(1, studentRecord1.getId());
        courseService.subscribeStudentToCourse(1, studentRecord2.getId());
        courseService.subscribeStudentToCourse(1, studentRecord3.getId());
        courseService.subscribeStudentToCourse(1, studentRecord4.getId());
    }

    @Test(expected = ApiException.class)
    public void testCourseAlreadySubscribe_should_return_error() throws ApiException {
        Student studentRecord11 = new Student("studentFirstName11", "studentLastName11", "studentEmail11@gmail.com");
        studentRepository.save(studentRecord11);
        courseService.subscribeStudentToCourse(1, studentRecord11.getId());
        courseService.subscribeStudentToCourse(1, studentRecord11.getId());
    }
}
