package com.registration.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.registration.system.controller.StudentController;
import com.registration.system.model.Student;
import com.registration.system.service.StudentService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StudentController.class)
public class StudentControllerTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    private StudentService studentService;

    Student studentRecord1 = new Student(1, "studentFirstName1", "studentLastName1", "studentEmail1@gmail.com");
    Student studentRecord2 = new Student(2, "studentFirstName2", "studentLastName2", "studentEmail2@gmail.com");
    Student studentRecord3 = new Student(3, "studentFirstName3", "studentLastName3", "studentEmail3@gmail.com");

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void test_getAllStudents() throws Exception {
        Mockito.when(studentService.getAllStudents()).thenReturn(Arrays.asList(studentRecord1, studentRecord2, studentRecord3));

        mvc.perform(MockMvcRequestBuilders
        .get("/students")
        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].firstName", is("studentFirstName1")))
                .andExpect(jsonPath("$[0].lastName", is("studentLastName1")))
                .andExpect(jsonPath("$[0].email", is("studentEmail1@gmail.com")))
                .andExpect(jsonPath("$[1].firstName", is("studentFirstName2")))
                .andExpect(jsonPath("$[1].lastName", is("studentLastName2")))
                .andExpect(jsonPath("$[1].email", is("studentEmail2@gmail.com")))
                .andExpect(jsonPath("$[2].firstName", is("studentFirstName3")))
                .andExpect(jsonPath("$[2].lastName", is("studentLastName3")))
                .andExpect(jsonPath("$[2].email", is("studentEmail3@gmail.com")));

    }

}
