INSERT INTO STUDENT(ID, FIRSTNAME, LASTNAME, EMAIL) VALUES
(1,'JOHN','DOE','johndoe@mail.com'),
(2,'MICHAEL','JORDAN','michaeljordan@mail.com'),
(3,'KEVIN','DURANT','kevindurant@mail.com'),
(4,'DAVID','JOHN','davidjohn@mail.com'),
(5,'LIAM','TAYLOR','liamtaylor@mail.com'),
(6,'NOAH','DAVIES','noadavies@mail.com'),
(7,'OLIVER','JOHN','oliverjohn@mail.com'),
(8,'EMI','WILSON','emiwilson@mail.com'),
(9,'ELI','EVANS','elievans@mail.com'),
(10,'EDD','DAVE','edddave@mail.com');

INSERT INTO COURSE(ID, COURSENAME, DESCRIPTION) VALUES
(1, 'MATH01', 'Mathematics basic algebra'),
(2, 'COMSCI01', 'Data structure and Algorithms'),
(3, 'PHYSICS01', 'Wave Energy'),
(4, 'LIT101', 'Literature and Arts'),
(5, 'PE101', 'Physical Education For Beginners');

INSERT INTO STUDENT_COURSE(STUDENT_ID, COURSE_ID) VALUES
(1,1),
(1,2),
(2,1),
(2,3),
(3,3);

--INSERT INTO COURSE_STUDENT(COURSE_ID, STUDENT_ID) VALUES
--(1,1),
--(2,1),
--(1,2),
--(3,2),
--(3,3);
