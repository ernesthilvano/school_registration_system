package com.registration.system.controller;

import com.registration.system.exception.ApiException;
import com.registration.system.request.CourseRequest;
import com.registration.system.service.CourseService;
import com.registration.system.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    CourseService courseService;

    @Autowired
    StudentService studentService;

    @GetMapping
    public ResponseEntity getAllCourse() {
        return ResponseEntity.ok(courseService.getAllCourses());
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity saveCourse(@Valid @RequestBody CourseRequest courseRequest) {
        return ResponseEntity.ok(courseService.saveCourse(courseRequest));
    }

    @GetMapping(path = "/{course_id}", produces = "application/json")
    public ResponseEntity getCourseById(@PathVariable("course_id") int id) {
        return ResponseEntity.ok(courseService.getCourseById(id));
    }

    @DeleteMapping(path = "/{course_id}", produces = "application/json")
    public ResponseEntity deleteCourse(@PathVariable("course_id") int id) throws ApiException {
        courseService.deleteCourse(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{course_id}", produces = "application/json")
    public ResponseEntity updateCourse(@PathVariable("course_id") int id, @Valid @RequestBody CourseRequest courseRequest) {
        return ResponseEntity.ok(courseService.updateCourse(id, courseRequest));
    }

    @PostMapping(path = "{course_id}/students/{student_id}", produces = "application/json")
    public ResponseEntity subscribeStudentToCourse(@PathVariable("course_id") int courseId,
                                                   @PathVariable("student_id") int studentId) throws ApiException {
        return ResponseEntity.ok(courseService.subscribeStudentToCourse(courseId, studentId));
    }

    @DeleteMapping(path = "{course_id}/students/{student_id}", produces = "application/json")
    public ResponseEntity deleteStudentFromCourse(@PathVariable("course_id") int courseId,
                                                  @PathVariable("student_id") int studentId) throws ApiException {
        return ResponseEntity.ok(courseService.deleteStudentFromCourse(courseId, studentId));
    }

    @GetMapping(path = "/students/{student_id}", produces = "application/json")
    public ResponseEntity getCourseByStudentId(@PathVariable("student_id") int studentId) {
        return ResponseEntity.ok(studentService.getCourseByStudentId(studentId));
    }

    @GetMapping(path = "/no_students", produces = "application/json")
    public ResponseEntity getCourseByNoStudents() {
        return ResponseEntity.ok(courseService.getCourseByNoStudents());
    }
}
