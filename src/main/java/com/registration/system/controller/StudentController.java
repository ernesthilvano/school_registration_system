package com.registration.system.controller;

import com.registration.system.exception.ApiException;
import com.registration.system.model.Course;
import com.registration.system.model.Student;
import com.registration.system.request.StudentRequest;
import com.registration.system.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/students")
public class StudentController {

	@Autowired
	private StudentService studentService;

	@GetMapping
	public ResponseEntity getAllStudents() {
		List<Student> students = studentService.getAllStudents();
		return ResponseEntity.ok(students);
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity saveStudent(@Valid @RequestBody StudentRequest studentRequest) {
		Student student = studentService.saveStudent(studentRequest);
		return ResponseEntity.ok(student);
	}

	@GetMapping(path = "/{student_id}", produces = "application/json")
	public ResponseEntity<Student> getStudentById(@PathVariable("student_id") int id) {
		return ResponseEntity.ok(studentService.getStudentById(id));
	}

	@DeleteMapping(path = "/{student_id}")
	public ResponseEntity deleteStudent(@PathVariable("student_id") int id) throws ApiException {
		studentService.deleteStudent(id);
		return ResponseEntity.ok().build();
	}

	@PutMapping(path = "/{student_id}", produces = "application/json")
	public ResponseEntity updateStudent(@PathVariable("student_id") int id, @Valid @RequestBody StudentRequest studentRequest) {
		return ResponseEntity.ok(studentService.updateStudent(id, studentRequest));
	}

	@GetMapping(path = "/{student_id}/courses", produces = "application/json")
	public ResponseEntity getCourseByStudentId(@PathVariable("student_id") int studentId) {
		Set<Course> courses = studentService.getCourseByStudentId(studentId);
		return ResponseEntity.ok(courses);
	}
	
	@GetMapping(path = "/courses/{course_id}", produces = "application/json")
	public ResponseEntity getStudentsSubscribed(@PathVariable("course_id") int courseId) {
		return ResponseEntity.ok(studentService.getStudentsSubscribed(courseId));
	}

	@GetMapping(path = "/no_courses", produces = "application/json")
	public ResponseEntity getStudentsNotSubscribed() {
		return ResponseEntity.ok(studentService.getStudentsNotSubscribed());
	}

}
