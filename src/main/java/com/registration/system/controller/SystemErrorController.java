package com.registration.system.controller;

import com.registration.system.response.ErrorResponse;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class SystemErrorController implements ErrorController {

    @RequestMapping("/error")
    public ResponseEntity handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Integer statusCode = Integer.valueOf(status.toString());
        String message = "Internal Server Error";
        ErrorResponse errorResponse = new ErrorResponse();

        if (statusCode == HttpStatus.NOT_FOUND.value()) {
            message = "Path not found";
        }

        errorResponse.setMessage(message);
        return ResponseEntity.status(statusCode).body(errorResponse);

    }
}
