package com.registration.system.repository;

import com.registration.system.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

	List<Student> findStudentsByCoursesIsNull();
	Student findStudentByEmailIgnoreCase(String email);
}
