package com.registration.system.exception;

/**
 * Represents Course related operation exceptions
 * e.g error presentation when course already reach student limit
 * and when student is being subscribe twice to the course
 */
public class ApiException extends Exception {


    public ApiException(String message) {
        super(message);
    }
}
