package com.registration.system.handler;

import com.registration.system.exception.ApiException;
import com.registration.system.response.ErrorModel;
import com.registration.system.response.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Method that check against {@code @Valid} Objects passed to controller endpoints
     *
     * @return a {@code ErrorResponse}
     * @see com.registration.system.response.ErrorResponse
     */
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<ErrorModel> errorMessages = exception.getBindingResult().getFieldErrors().stream()
                .map(err -> new ErrorModel(err.getField(), err.getRejectedValue(), err.getDefaultMessage()))
                .distinct()
                .collect(Collectors.toList());
        ErrorResponse errorResponse = new ErrorResponse(errorMessages);
        errorResponse.setStatus(HttpStatus.BAD_REQUEST);
        errorResponse.setMessage("Validation Error" + (errorMessages.size() == 1 ? "" : "s"));
        return ResponseEntity.badRequest().body(errorResponse);
    }

    @ExceptionHandler({ EntityNotFoundException.class })
    protected ResponseEntity<Object> handleEntityNotFound(
            EntityNotFoundException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(HttpStatus.NOT_FOUND);
        errorResponse.setMessage("Entity not found");
        errorResponse.setDebugMessage(ex.getMessage());
        return ResponseEntity.badRequest().body(errorResponse);
    }

    @ExceptionHandler({ EntityExistsException.class })
    protected ResponseEntity<Object> handleEntityExist(
            EntityExistsException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(HttpStatus.BAD_REQUEST);
        errorResponse.setMessage("Entity already exist");
        return ResponseEntity.badRequest().body(errorResponse);
    }

    @ExceptionHandler({ ApiException.class })
    protected ResponseEntity<Object> handleApiException(
            ApiException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(HttpStatus.BAD_REQUEST);
        errorResponse.setMessage("Unable to process request");
        errorResponse.setDebugMessage(ex.getMessage());
        return ResponseEntity.badRequest().body(errorResponse);
    }



}
