package com.registration.system.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.registration.system.request.CourseRequest;

import javax.persistence.*;
import java.util.Set;

/**
 * Represents a course that can be subscribed by students.
 * A course allows configurable limit of maximum students.
 */
@Entity
@Table(name = "COURSE")
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "COURSENAME")
	private String courseName;
	@Column(name = "DESCRIPTION")
	private String description;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "student_course", joinColumns = @JoinColumn(name = "course_id", referencedColumnName = "id"),
	inverseJoinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"))
	@JsonIgnore
	private Set<Student> students;

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Course() {

	}

	public Course(int id, String courseName, String description, int cfu) {
		super();
		this.id = id;
		this.courseName = courseName;
		this.description = description;
	}

	public void setAttributes(CourseRequest courseRequest) {
		this.courseName = courseRequest.getName();
		this.description = courseRequest.getDescription();
	}

	public void subscribeStudent(Student student) {
		students.add(student);
	}

	public void deleteStudent(Student student) {
		students.remove(student);
	}

}
