package com.registration.system.service;

import com.registration.system.exception.ApiException;
import com.registration.system.model.Course;
import com.registration.system.model.Student;
import com.registration.system.repository.CourseRepository;
import com.registration.system.repository.StudentRepository;
import com.registration.system.request.StudentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Contains all student related operations.
 * This class contains the filtering and CRUD functionality
 */
@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepo;

    @Autowired
    CourseRepository courseRepo;

    public List<Student> getAllStudents() {
        return studentRepo.findAll();
    }

    /**
     * Creates new student that can be enrolled to courses
     * This method returns error when student email already exists
     *
     * @throws EntityExistsException    if email already exists
     * @param studentRequest            the sstudent to be created
     * @return                          the created student
     */
    public Student saveStudent(StudentRequest studentRequest) {
        if (studentRepo.findStudentByEmailIgnoreCase(studentRequest.getEmail()) != null)  {
            throw new EntityExistsException();
        }

        Student student = new Student();
        student.setFirstName(studentRequest.getFirstName());
        student.setLastName(studentRequest.getLastName());
        student.setEmail(studentRequest.getEmail());

        return studentRepo.save(student);
    }

    public Student getStudentById(int id) {
        Optional<Student> student = studentRepo.findById(id);

        if (student.isPresent()) {
            return student.get();
        } else {
            throw new EntityNotFoundException();
        }
    }

    public void deleteStudent(int id) throws ApiException {
        Optional<Student> student = studentRepo.findById(id);
        if (student.isPresent()) {
            if (!student.get().getCourses().isEmpty()) {
                throw new ApiException("Student has course subscribed. unable to delete");
            }
            studentRepo.deleteById(id);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public Student updateStudent(int id, StudentRequest studentRequest) {
        Optional<Student> student = studentRepo.findById(id);
        if (student.isPresent()) {
            Student oldStudent = student.get();
            oldStudent.setAttributes(studentRequest);
            studentRepo.save(oldStudent);
            return oldStudent;
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * Retrieves all courses by student
     * @param id       Student id
     * @return         Courses by student
     */
    public Set<Course> getCourseByStudentId(int id) {
        Optional<Student> student = studentRepo.findById(id);
        if (student.isPresent()) {
            return student.get().getCourses();
        } else {
            throw new EntityNotFoundException();
        }

    }

    /**
     * Retrieves all students from course;
     * @param courseId the course ID
     * @return          students from course
     */
    public Set<Student> getStudentsSubscribed(int courseId) {
        Optional<Course> course = courseRepo.findById(courseId);
        if (course.isPresent()) {
            return course.get().getStudents();
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * Retrieves student that doesnt have course subscribed.
     *
     * @return Students with no course
     */
    public List<Student> getStudentsNotSubscribed() {
        return studentRepo.findStudentsByCoursesIsNull();
    }
}
