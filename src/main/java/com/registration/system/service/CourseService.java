package com.registration.system.service;

import com.registration.system.exception.ApiException;
import com.registration.system.model.Course;
import com.registration.system.model.Student;
import com.registration.system.repository.CourseRepository;
import com.registration.system.repository.StudentRepository;
import com.registration.system.request.CourseRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * Contains all course related operations.
 * This class contains the filtering and CRUD functionality
 */
@Service
public class CourseService {

    @Value("${max.student.per.course}")
    private int maxStudentPerCourse;

    @Value("${max.course.per.student}")
    private int maxCoursePerStudent;

    @Autowired
    StudentRepository studentRepo;

    @Autowired
    CourseRepository courseRepo;

    /**
     * Retrieves all courses
     * @return List of Courses
     */
    public List<Course> getAllCourses() {
        return courseRepo.findAll();
    }

    /**
     * Creates new course that can be enrolled by students
     * This method returns error when course name already exists
     *
     * @throws EntityExistsException    if name already exists
     * @param courseRequest             the course to be created
     * @return                          the created course
     */
    public Course saveCourse(CourseRequest courseRequest) {
        if (courseRepo.findByCourseName(courseRequest.getName()).isPresent())  {
            throw new EntityExistsException();
        }

        Course course = new Course();
        course.setCourseName(courseRequest.getName());
        course.setDescription(courseRequest.getDescription());
        return courseRepo.save(course);
    }

    /**
     * Retrieves course by id
     *
     * @param id                        course Id
     * @return                          course
     * @throws EntityNotFoundException  error when course id is not found
     */
    public Course getCourseById(int id) {
        Optional<Course> course = courseRepo.findById(id);
        if (course.isPresent()) {
            return course.get();
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * Updates course by id and replaces all attributes
     * defined from courseRequest
     *
     * @param courseRequest             Course request containing fields to be updated
     * @return                          Updated course
     * @throws EntityNotFoundException  error when course id is not found
     */
    public Course updateCourse(int id, CourseRequest courseRequest) {
        Optional<Course> course = courseRepo.findById(id);
        if (course.isPresent()) {
            Course oldCourse = course.get();
            oldCourse.setAttributes(courseRequest);
            courseRepo.save(oldCourse);
            return oldCourse;
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * Deletes course if there is no student subscribed.
     * This method returns an error if course id is invalid
     *
     * @param id            Course Id to be deleted
     * @return              ResponseEntity with status OK
     * @throws ApiException Error when course has students currently
     */
    public ResponseEntity deleteCourse(int id) throws ApiException {
        Optional<Course> course = courseRepo.findById(id);
        if (course.isPresent()) {
            if (!course.get().getStudents().isEmpty()) {
                throw new ApiException("Course currently has students. unable to delete");
            }
            courseRepo.deleteById(id);
            return ResponseEntity.ok().build();
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * This method subscribes student to course
     * Error will be returned when student or course id is invalid
     * and when course already reached the limit of students
     * and when student is already a subscriber of the course
     *
     * @param courseId      Course Id
     * @param studentId     Student Id
     * @return              Course being subscribed
     * @throws ApiException Error when student is already subscribed or course over subscribed
     */
    public Course subscribeStudentToCourse(int courseId, int studentId) throws ApiException {
        Optional<Course> courseOptional = courseRepo.findById(courseId);

        // fail fast check if course has slot
        if (!courseOptional.isPresent()) {
            throw new EntityNotFoundException("Course id not found");
        } else if (courseOptional.get().getStudents().size() >= maxStudentPerCourse) {
            throw new ApiException("Course already full of students");
        }

        Optional<Student> studentOptional = studentRepo.findById(studentId);

        if (!studentOptional.isPresent()) {
            throw new EntityNotFoundException("Student id not found");
        } else {
            if (studentOptional.get().getCourses().size() >= maxStudentPerCourse) {
                throw new ApiException("Student reached course limit");
            }
        }

        Course course = courseOptional.get();
        Student student = studentOptional.get();

        for (Student s : course.getStudents()) {
            if (s.getId() == studentId) {
                throw new ApiException("Student already subscribed");
            }
        }

        course.subscribeStudent(student);
        courseRepo.save(course);
        return course;
    }

    /**
     * This method deletes students from the course
     * Error will be returned when courseId or studentId is invalid.
     * Error will also be raised when the course does not currently have the student
     *
     * @param courseId          the course id
     * @param studentId         the student id to be deleted from course
     * @return                  the course object
     * @throws ApiException     error when course does not currently have the student
     */
    public Course deleteStudentFromCourse(int courseId,
                                          int studentId) throws ApiException {

        Optional<Course> courseOptional = courseRepo.findById(courseId);
        Optional<Student> studentOptional = studentRepo.findById(studentId);
        if (!courseOptional.isPresent()) {
            throw new EntityNotFoundException("Course id not found");
        }

        if (!studentOptional.isPresent()) {
            throw new EntityNotFoundException("Student id not found");
        }

        Course course = courseOptional.get();

        Student student = studentOptional.get();
        if (!course.getStudents().contains(student)) {
            throw new ApiException("Course does not contain student with id " + studentId);
        }
        course.deleteStudent(student);
        courseRepo.save(course);
        return course;
    }

    /**
     * Retrieves all courses that has no students
     *
     * @return  List of course where there is no students subscribed
     */
    public List<Course> getCourseByNoStudents() {
        return courseRepo.findByStudentsIsNull();
    }
}
