package com.registration.system.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class StudentRequest {

    public StudentRequest(){};

    public StudentRequest(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    @NotBlank(message = "Firstname is mandatory")
    @Size(min = 2, max = 30, message = "First name should be between 2 to 30 characters")
    private String firstName;

    @NotBlank(message = "Lastname is mandatory")
    @Size(min = 2, max = 30, message = "Last name should be between 2 to 30 characters")
    private String lastName;

    @NotBlank(message = "Email is mandatory")
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", flags = Pattern.Flag.CASE_INSENSITIVE, message = "Invalid email format")
    @Size(min = 3, max = 32, message = "Email should be between 3 to 32 characters")
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
