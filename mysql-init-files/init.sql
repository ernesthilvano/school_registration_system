CREATE DATABASE IF NOT EXISTS registration_system;
CREATE USER 'newuser'@'%' IDENTIFIED BY 'password';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, INDEX, DROP, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON `registration_system`.* TO 'newuser'@'%';
FLUSH PRIVILEGES;

USE registration_system;

CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coursename` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `student_course` (
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`course_id`,`student_id`)
);

INSERT INTO student(id, firstname, lastname, email) VALUES
(1,'JOHN','DOE','johndoe@mail.com'),
(2,'MICHAEL','JORDAN','michaeljordan@mail.com'),
(3,'KEVIN','DURANT','kevindurant@mail.com'),
(4,'DAVID','JOHN','davidjohn@mail.com'),
(5,'LIAM','TAYLOR','liamtaylor@mail.com'),
(6,'NOAH','DAVIES','noadavies@mail.com'),
(7,'OLIVER','JOHN','oliverjohn@mail.com'),
(8,'EMI','WILSON','emiwilson@mail.com'),
(9,'ELI','EVANS','elievans@mail.com'),
(10,'EDD','DAVE','edddave@mail.com');

INSERT INTO course(id, coursename, description) VALUES
(1, 'MATH01', 'Mathematics basic algebra'),
(2, 'COMSCI01', 'Data structure and Algorithms'),
(3, 'PHYSICS01', 'Wave Energy'),
(4, 'LIT101', 'Literature and Arts'),
(5, 'PE101', 'Physical Education For Beginners');

INSERT INTO student_course(student_id, course_id) VALUES
(1,1),
(1,2),
(2,1),
(2,3),
(3,3);
USE registration_system;